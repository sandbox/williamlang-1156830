<?php

/**
 * @file
 * Template for displaying games and leaderboards
 */
if ($game) {
  echo '<h2>Games</h2>';

  if ($games) {
    foreach ($games as $g) {
      echo $g->name . '<br />';
    }
  } else {
    echo t('No games have been added.');
  }
}

if ($leaderboard) {
  echo '<h2>Leaderboards</h2>';

  if ($leaderboards) {
    foreach ($leaderboards as $l) {
      echo l($l->name, LADDER_LEADERBOARDS_PATH . $l->lid);
    }
  } else {
    echo t('No leaderboards have been created.');
  }
}