<?php
/**
 * @file
 * Template file to display links to leaderboards
 */
foreach ($leaderboards as $l) {
  echo l($l->name, LADDER_LEADERBOARDS_PATH . $l->lid);
}
?>