<?php
/**
 * @file
 * Template for individual leaderboards
 */
if (!$leaderboard) {
  echo t('Leaderboard not found');
} else {
?>
  <h2><?php echo $leaderboard['name']; ?></h2>

  <p>
    <strong><?php echo t('Game'); ?>:</strong> <?php echo $games[$leaderboard['gid']]->name; ?><br />
    <strong><?php echo t('Opening Day'); ?>:</strong> <?php echo date(LADDER_LEADERBOARDS_DATE_FORMAT, $leaderboard['start']); ?><br />
    <strong><?php echo t('Closing Day'); ?>:</strong> <?php echo date(LADDER_LEADERBOARDS_DATE_FORMAT, $leaderboard['end']); ?><br />
  </p>

<?php
  $now = time();
  if ($now > $leaderboard['start'] && $now < $leaderboard['end']) {
?>
    <p><?php echo l('Report a Result', LADDER_LEADERBOARDS_PATH . $leaderboard['lid'] . '/report'); ?></p>
<?php
  }
?>

  <h2><?php echo t('Standings'); ?></h2>

<?php
  if (isset($standings) && sizeof($standings) > 0) {
?>
    <table>
      <tr>
        <th><?php echo t('Rank'); ?></th>
        <th><?php echo t('Competitor'); ?></th>
        <th><?php echo t('Wins'); ?></th>
        <th><?php echo t('Losses'); ?></th>
      </tr>
  <?php
    $rank = 0;
    foreach ($standings as $uid => $result) {
      $rank++;
  ?>
      <tr>
        <td><?php echo $rank; ?></td>
        <td><?php echo l($result['name'], 'user/' . $result['uid']); ?></td>
        <td><?php echo $result['wins']; ?></td>
        <td><?php echo $result['losses']; ?></td>
      </tr>
  <?php
    }
  ?>
  </table>
<?php
  } else {
?>
    <p><?php echo t('There are currently no standings.'); ?></p>
<?php
  }
}
?>