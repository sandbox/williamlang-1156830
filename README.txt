Ladder

Ladder is a module to allow the creation of standings (synonym: leaderboards, rankings)

After you install Ladder Games and Ladder Leaderboards you will be able to add a game.

A game can be anything from StarCraft II to a hockey league.

After you have added a game you can create a leaderboard for a game (up to as many as you want)

The start date is the day users can start reporting outcomes of matches.

The end date is the day users can no longer report outcomes of matches.

There are individual permissions for the view, creation and reporting of matches for leaderboards.
